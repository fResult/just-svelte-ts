declare module svelte.JSX {
  interface HTMLAttributes<T> {
    onlongpress?: (event?: CustomEvent) => void
  }
}
