declare namespace svelte.JSX {
  interface HTMLAttributes<T> {
    onpanend?: (event?: CustomEvent) => void,
    onpanstart?: (event: CustomEvent) => void,
    onpanmove?: (event: CustomEvent) => void,
  }
}
