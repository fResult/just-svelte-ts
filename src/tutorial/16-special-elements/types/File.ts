export type IFile = {
  name?: string
  type?: string
  files?: Array<IFile>
}
