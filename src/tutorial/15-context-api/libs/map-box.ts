import mapbox from 'mapbox-gl'

// https://docs.mapbox.com/help/glossary/access-token/
mapbox.accessToken = <string>import.meta.env.VITE_MAPBOX_ACCESS_TOKEN

const key = {}

export { mapbox, key }
