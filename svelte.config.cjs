const sveltePreprocess = require('svelte-preprocess')
const pkg = require('./package.json')
const DEV = process.env.NODE_ENV === 'development'
// const createPreprocessor = ({ sourceMap }) => (
//   sveltePreprocess({
//       sourceMap,
//       default: {
//         script: 'typescript',
//         style: 'postcss'
//       }
//     }
//   ))

module.exports = {
  // Consult https://github.com/sveltejs/svelte-preprocess
  // for more information about preprocessors
  // preprocess: createPreprocessor({ sourceMap: true })
  replaces: [
    [/__VERSION__/gim, DEV ? `- เวอร์ชั่น: ${pkg.version}` : '']
  ],
  preprocess: sveltePreprocess()

}
